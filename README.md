# ssplit

Small utility splitting stdin stream to rotating files

Usage: `npx ssplit -o <file pattern> -z <zip pattern> [options]`

```
Options:
  --help           Show help                                           [boolean]
  --version        Show version number                                 [boolean]
  --stderr-output  outputs to stderr instead of stdout                 [boolean]
  --silent         disables stdout/stderr output                       [boolean]
  --lines, -l      keeps at most this number of lines per file
                                                        [number] [default: 1000]
  --output, -o     file to output the input to. Can contain special markers:
                   - %N: file index
                   - %D: date when the file is created. Format is YYYY-MM-DD
                   - %T: time when the file is created. Format is HH-mm-ss
                   - %M: milliseconds when the file is created
                                                             [string] [required]
  --zip, -z        zip file to make when a cycle restarts. Can contain special
                   markers:
                   - %D: date when the file is created. Format is YYYY-MM-DD
                   - %T: time when the file is created. Format is HH-mm-ss
                   - %M: milliseconds when the file is created
                                                             [string] [required]
  --digits, -d     file indexes will contain this number of digits
                                                           [number] [default: 2]
```

### Example:

`./my_app | npx ssplit -o /var/log/my_app_%N_%D_%T.%M.txt --silent -z /var/log/zip/my_app_%D_%T.%M.zip -d 1 -l 100`

Will automatically split the `my_app` logs on every `\n` so that there is at most 100 lines per `output` file.  
Every 100 lines, a new `output` file is created with its `%N` increased by one.  
The `-d` option specified how many digits to keep for the `%N` value. This determines how many files will be created at most.  
With `-d 2` (default), there will be 100 files (`%N` will vary from `00` to `99`).  
Once every `%N` value is exhausted, `ssplit` will zip every file and start over.
