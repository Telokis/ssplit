const { Writable } = require("stream");
const FileStream = require("./FileStream");

class SsplitStream extends Writable {
    constructor(args, options) {
        super(options);

        this.outStreams = [];

        let targetStream = process.stdout;

        if (args["stderr-output"]) {
            targetStream = process.stderr;
        }

        if (!args.silent) {
            this.outStreams.push(targetStream);
        }

        this.outStreams.push(new FileStream(args.output, args));
    }

    _write(chunk, encoding, callback) {
        for (let i = 0; i < this.outStreams.length; ++i) {
            this.outStreams[i].write(chunk);
        }

        callback();
    }
}

module.exports = SsplitStream;
