const { Writable } = require("stream");
const fs = require("fs");
const path = require("path");
const fileFlags = require("./fileFlags");
const {
    ensureDirectoryExists,
    pad,
    getDate,
    getTime,
    escapeRegExp,
    getMilliseconds,
    zipFiles,
    delay,
} = require("./helpers");

class FileStream extends Writable {
    constructor(output, args, options) {
        super(options);

        this.fileMaxIndex = 10 ** args.digits;
        this.output = output;
        this.args = args;
        this.lines = 0;
        this.fileIndex = -1;
        this.writeStream = null;

        this.dirPath = path.dirname(path.resolve(process.cwd(), this.output));

        try {
            ensureDirectoryExists(this.dirPath);
            ensureDirectoryExists(path.dirname(path.resolve(process.cwd(), this.args.zip)));
        } catch (e) {
            console.error("===================== ERROR =====================");
            console.error("");
            console.error("An error occurred while trying to create the directory");
            console.error(this.dirPath);
            console.error("");
            console.error(e.message);
            console.error("");
            console.error("=================================================");
            throw e;
        }

        const directoryContent = fs.readdirSync(this.dirPath);
        const flagEntries = Object.entries(fileFlags);
        const replaced = flagEntries.reduce(
            (str, [flag, { checkRegex }]) =>
                str.replace(new RegExp(flag, "g"), checkRegex(this.args)),
            escapeRegExp(path.basename(this.output)),
        );
        this.patternRegex = new RegExp(`^${replaced}$`);

        for (let i = 0; i < directoryContent.length; i++) {
            const file = directoryContent[i];
            const match = file.match(this.patternRegex);

            if (match) {
                const index = parseInt(match.groups.index, 10);

                if (index >= this.fileIndex) {
                    this.fileIndex = index;
                }
            }
        }
    }

    async createWriteStream() {
        const now = new Date();
        this.fileIndex += 1;

        if (this.writeStream) {
            this.writeStream.end();
            this.writeStream = null;
        }

        if (this.fileIndex >= this.fileMaxIndex) {
            await delay(100);
            const filesToZip = fs
                .readdirSync(this.dirPath)
                .filter(file => this.patternRegex.test(file))
                .map(file => path.resolve(this.dirPath, file));
            const zipPath = path.resolve(
                process.cwd(),
                this.args.zip
                    .replace(/%D/g, getDate(now))
                    .replace(/%T/g, getTime(now))
                    .replace(/%M/g, getMilliseconds(now)),
            );

            await zipFiles(filesToZip, zipPath);
            await Promise.all(filesToZip.map(file => fs.promises.unlink(file)));

            this.fileIndex = 0;
        }

        const filePath = path.resolve(
            process.cwd(),
            this.output
                .replace(/%N/g, pad(this.fileIndex, this.args.digits))
                .replace(/%D/g, getDate(now))
                .replace(/%T/g, getTime(now))
                .replace(/%M/g, getMilliseconds(now)),
        );

        this.writeStream = fs.createWriteStream(filePath);
    }

    _writeHelper(chunk) {
        this.writeStream.write(chunk);
    }

    async _write(chunk, encoding, callback) {
        if (!this.writeStream) {
            await this.createWriteStream();
        }

        const str = chunk.toString();
        const lines = str.split("\n");
        const linesLeft = this.args.lines - this.lines;

        if (lines.length <= linesLeft) {
            this._writeHelper(chunk);
            this.lines += lines.length;
        } else {
            let i = linesLeft;

            this._writeHelper(lines.slice(0, linesLeft).join("\n"));

            while (i < lines.length) {
                await this.createWriteStream();
                this._writeHelper(lines.slice(i, i + linesLeft).join("\n"));
                i += this.args.lines;
            }
        }

        callback();
    }
}

module.exports = FileStream;
