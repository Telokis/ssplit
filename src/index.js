const args = require("./args");
const SsplitStream = require("./SsplitStream");

const ssplitStream = new SsplitStream(args);

process.stdin.pipe(ssplitStream);
