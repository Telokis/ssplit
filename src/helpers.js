const fs = require("fs");
const archiver = require("archiver");
const path = require("path");

exports.ensureDirectoryExists = function ensureDirectoryExists(dirPath) {
    if (!fs.existsSync(dirPath)) {
        fs.mkdirSync(dirPath, { recursive: true });
    }
};

function pad(num, padLength, padStr = "0") {
    return String(num).padStart(padLength, padStr);
}
exports.pad = pad;

exports.getDate = function getDate(date) {
    return `${date.getFullYear()}-${pad(date.getMonth(), 2)}-${pad(date.getDate() + 1, 2)}`;
};

exports.getTime = function getTime(date) {
    return `${pad(date.getHours(), 2)}-${pad(date.getMinutes(), 2)}-${pad(date.getSeconds(), 2)}`;
};

exports.getMilliseconds = function getMilliseconds(date) {
    return `${pad(date.getMilliseconds(), 3)}`;
};

exports.escapeRegExp = function escapeRegExp(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
};

exports.zipFiles = function zipFiles(files, zipPath) {
    const archive = archiver("zip", { zlib: { level: 9 } });
    const stream = fs.createWriteStream(zipPath);

    return new Promise((resolve, reject) => {
        for (let i = 0; i < files.length; i++) {
            const file = files[i];

            archive.file(file, { name: path.basename(file) });
        }

        archive.on("error", err => reject(err)).pipe(stream);

        stream.on("close", () => resolve());
        archive.finalize();
    });
};

exports.delay = function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};
