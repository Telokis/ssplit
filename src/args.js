const yargs = require("yargs");
const path = require("path");
const fileFlags = require("./fileFlags");

const args = yargs
    .usage("Usage: $0")
    .option("stderr-output", {
        describe: "outputs to stderr instead of stdout",
        type: "boolean",
        demandOption: false,
    })
    .option("silent", {
        describe: "disables stdout/stderr output",
        type: "boolean",
        demandOption: false,
    })
    .option("lines", {
        describe: "keeps at most this number of lines per file",
        type: "number",
        demandOption: false,
        default: 1000,
        alias: ["l"],
    })
    .option("output", {
        describe: `file to output the input to. Can contain special markers:\n${Object.entries(
            fileFlags,
        )
            .map(([name, { description }]) => `- ${name}: ${description}`)
            .join("\n")}`,
        type: "string",
        demandOption: true,
        alias: ["o"],
    })
    .option("zip", {
        describe: `zip file to make when a cycle restarts. Can contain special markers:\n${Object.entries(
            fileFlags,
        )
            .filter(([, { zip }]) => zip)
            .map(([name, { description }]) => `- ${name}: ${description}`)
            .join("\n")}`,
        type: "string",
        demandOption: true,
        alias: ["z"],
    })
    .option("digits", {
        describe: "file indexes will contain this number of digits",
        type: "number",
        default: 2,
        alias: ["d"],
        demandOption: false,
    }).argv;

if (args.lines <= 0) {
    throw new Error("lines option must be strictly positive.");
}

if (args.digits <= 0) {
    throw new Error("a option must be strictly positive.");
}

const fileFlagRegex = new RegExp(Object.keys(fileFlags).join("|"));

const out = args.output;
const dirPath = path.dirname(out);

if (fileFlagRegex.test(dirPath)) {
    throw new Error("Directory path cannot contain %D, %N or %T");
}

if (out.indexOf("%N") < 0) {
    args.output += "%N";
}

module.exports = args;
