module.exports = {
    "%N": {
        description: "file index",
        checkRegex: args => `(?<index>[0-9]{${args.digits}})`,
        zip: false,
    },
    "%D": {
        description: "date when the file is created. Format is YYYY-MM-DD",
        checkRegex: () => "[0-9]{4}-[0-9]{2}-[0-9]{2}",
        zip: true,
    },
    "%T": {
        description: "time when the file is created. Format is HH-mm-ss",
        checkRegex: () => "[0-9]{2}-[0-9]{2}-[0-9]{2}",
        zip: true,
    },
    "%M": {
        description: "milliseconds when the file is created",
        checkRegex: () => "[0-9]{3}",
        zip: true,
    },
};
