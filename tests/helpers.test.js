const { pad } = require("../src/helpers");

describe("pad", () => {
    it("should add zeroes properly for the basic case", () => {
        expect(pad(3, 3, "0")).toStrictEqual("003");
    });

    it("should pad with more than one character", () => {
        expect(pad(3, 4, "ab")).toStrictEqual("aba3");
    });

    it("should not pad if the length is smaller or equal", () => {
        expect(pad(33, 2, "0")).toStrictEqual("33");
    });
});
