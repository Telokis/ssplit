describe("Args", () => {
    beforeEach(() => {
        jest.resetModules();
        jest.resetAllMocks();
    });

    it("should only need -o and -z options", () => {
        process.argv = [
            "",
            "",
            "-o",
            "/var/log/my_app_%N_%D_%T.%M.txt",
            "-z",
            "/var/log/zip/my_app_%D_%T.%M.zip",
        ];

        const mockExit = jest.spyOn(process, "exit").mockImplementation(() => {});

        expect(() => {
            require("../src/args");
        }).not.toThrow();

        expect(mockExit).not.toHaveBeenCalled();
    });

    it("should throw if the required options are not provided", () => {
        process.argv = ["", ""];

        const mockExit = jest.spyOn(process, "exit").mockImplementation(() => {});

        expect(() => {
            require("../src/args");
        }).toThrow();

        expect(mockExit).toHaveBeenCalledTimes(1);
    });
});
